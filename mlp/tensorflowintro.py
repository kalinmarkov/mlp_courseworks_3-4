import tensorflow as tf
import mlp.data_providers as data_providers


def get_error_and_accuracy(data):
    """Calculate average error and classification accuracy across a dataset.
    
    Args:
        data: Data provider which iterates over input-target batches in dataset.
        
    Returns:
        Tuple with first element scalar value corresponding to average error
        across all batches in dataset and second value corresponding to
        average classification accuracy across all batches in dataset.
    """
    err = 0
    acc = 0
    for input_batch, target_batch in data:
        err += sess.run(error, feed_dict={inputs: input_batch, targets: target_batch})
        acc += sess.run(accuracy, feed_dict={inputs: input_batch, targets: target_batch})
    err /= data.num_batches
    acc /= data.num_batches
    return err, acc


inputs = tf.placeholder(tf.float32, [None, 784], 'inputs')
targets = tf.placeholder(tf.float32, [None, 10], 'targets')

weights = tf.Variable(tf.zeros([784, 10]))
biases = tf.Variable(tf.zeros([10]))

outputs = tf.matmul(inputs, weights) + biases

per_datapoint_errors = tf.nn.softmax_cross_entropy_with_logits(outputs, targets)
error = tf.reduce_mean(per_datapoint_errors)

per_datapoint_pred_is_correct = tf.equal(tf.argmax(outputs, 1), tf.argmax(targets, 1))
accuracy = tf.reduce_mean(tf.cast(per_datapoint_pred_is_correct, tf.float32))

train_step = tf.train.GradientDescentOptimizer(learning_rate=0.5).minimize(error)

sess = tf.InteractiveSession()


init_op = tf.global_variables_initializer()
sess.run(init_op)

train_data = data_providers.MNISTDataProvider('train', batch_size=50)
valid_data = data_providers.MNISTDataProvider('valid', batch_size=50)

print('Training Set')

num_epoch = 5
for e in range(num_epoch):
    running_error = 0.
    running_accuracy = 0.
    for input_batch, target_batch in train_data:
        _, batch_error, batch_accuracy = sess.run(
            [train_step, error, accuracy],
            feed_dict={inputs: input_batch, targets: target_batch})
        running_error += batch_error
        running_accuracy += batch_accuracy
    running_error /= train_data.num_batches
    running_accuracy /= train_data.num_batches
    print('End of epoch {0}: running error average= {1:.2f}, running accuracy average= {2:.2f}'.format(e + 1, running_error, running_accuracy))

print('\n')

print('Averages')

print('Train data: Error={0:.2f} Accuracy={1:.2f}'
      .format(*get_error_and_accuracy(train_data)))
print('Valid data: Error={0:.2f} Accuracy={1:.2f}'
      .format(*get_error_and_accuracy(valid_data)))