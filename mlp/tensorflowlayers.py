import os
import tensorflow as tf

"""
Class representing different possible layers in a neural network
that is created using the tensor flow libraries. There is no need
to define backpropagation methods because tensor flow automatically 
will do this when we call it in the main script.
"""

def fully_connected_layer_batch_norm(inputs, input_dim, output_dim, nonlinearity=tf.nn.relu):
    weights = tf.Variable(
        tf.truncated_normal(
            [input_dim, output_dim], stddev=2. / (input_dim + output_dim)**0.5), 
        'weights')
    # biases = tf.Variable(tf.zeros([output_dim]), 'biases')    # going ignore biases because we will always do batch normalization
    # going to apply batch normalization here
    outputs_no_bn = tf.matmul(inputs, weights)
    batch_mean, batch_var = tf.nn.moments(outputs_no_bn,[0])
    scale = tf.Variable(tf.ones([output_dim]))
    beta = tf.Variable(tf.zeros([output_dim]))
    epsilon = 1e-3
    outputs_bn = tf.nn.batch_normalization(outputs_no_bn,batch_mean,batch_var,beta,scale,epsilon)
    outputs = nonlinearity(outputs_bn)
    return outputs, weights

def fully_connected_layer(inputs, input_dim, output_dim, nonlinearity=tf.nn.relu):
    weights = tf.Variable(
        tf.truncated_normal(
            [input_dim, output_dim], stddev=2. / (input_dim + output_dim)**0.5), 
        'weights')
    biases = tf.Variable(tf.zeros([output_dim]), 'biases')    # going ignore biases because we will always do batch normalization
    outputs_no_activation = tf.matmul(inputs, weights) + biases
    outputs = nonlinearity(outputs_no_activation)
    return outputs, weights

def fully_connected_layer_cnn(inputs, weights, biases, nonlinearity=tf.nn.relu):
    outputs_no_activation = tf.matmul(inputs, weights) + biases  
    outputs = nonlinearity(outputs_no_activation)
    return outputs
