import os
import tensorflow as tf
import numpy as np
from mlp.data_providers import CIFAR10DataProvider, CIFAR100DataProvider
import matplotlib.pyplot as plt
import sys
import data_providers
import tensorflowlayers
import datetime
import cPickle as pickle
import time

# all global variables declared here
commandLineArguments = sys.argv
print(commandLineArguments)
dataset = commandLineArguments[1]
batch_size = commandLineArguments[2]
output_dimension = 0

global_mean = tf.zeros([3072],dtype=tf.float16)
global_sum = tf.zeros([3072],dtype=tf.float16)
global_n = tf.zeros([3072],dtype=tf.float16)
global_ones = tf.ones([3072],dtype=tf.float16)
global_int_counter = 0


patch_size = 5
hidden_dimension = 1500
kp = 0.8
num_channels1 = 32
num_channels2 = 16
isTrain = True

def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)

def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
  return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='SAME')

def batch_norm(x,output_dim,use_global=False,isInConv=True):
    epsilon = 1e-3
    outputs_bn = 0.0
    if(use_global == False):
        if(isInConv == True):
            batch_mean, batch_var = tf.nn.moments(x,[0,1,2],keep_dims=True)
        else:
            batch_mean, batch_var = tf.nn.moments(x,[0],keep_dims=True)
        print('Batch mean is ' + str(batch_mean))
        print('Batch var is ' + str(batch_var))
        scale = tf.Variable(tf.ones([output_dim]))
        beta = tf.Variable(tf.zeros([output_dim]))
        outputs_bn = tf.nn.batch_normalization(x,batch_mean,batch_var,beta,scale,epsilon)
    else:
        outputs_bn = tf.nn.batch_normalization(x,global_mean,global_variance,beta,scale,epsilon)
    return outputs_bn

"""
def online_variance(data):
    for x in tf.unpack(data,axis=1):
        print('x is ' + str(x))
        global global_int_counter, global_n, global_mean, global_sum, global_variance
        global_int_counter = global_int_counter + 1
        global_n_temp = tf.add(global_n, global_ones)
        global_n = global_n_temp
        delta = tf.subtract(x, global_mean)
        global_mean = tf.add(global_mean, tf.divide(delta,global_n))
        delta2 = tf.subtract(x,global_mean)
        global_sum = tf.add(global_sum, tf.multiply(delta,delta2))
        if global_int_counter >= 2:
            global_variance = tf.divide(global_sum,tf.subtract(global_n,global_ones))
"""

def fully_connected_layer_cnn(inputs, weights, biases, nonlinearity=tf.nn.relu):
    outputs_no_activation = tf.matmul(inputs, weights) + biases  
    outputs = nonlinearity(outputs_no_activation)
    return outputs

def fully_connected_layer_cnn_batch_norm(inputs, weights, biases, output_dim, nonlinearity=tf.nn.relu, is_on_train=True):
    outputs_no_activation = tf.matmul(inputs, weights) + biases   
    if(is_on_train == True):
        outputs_bn = batch_norm(outputs_no_activation,output_dim,isInConv=False)
    else:
        outputs_bn = batch_norm(outputs_no_activation,output_dim,use_global=True,isInConv=True)
    outputs = nonlinearity(outputs_no_activation)
    return outputs

if len(commandLineArguments) != 3:     # 3 because the file name is also an argument
    raise ValueError('Must have exactly 2 arguments - dataset and batch_size')


if(dataset == 'cifar10'):   
    train_data = CIFAR10DataProvider('train', int(batch_size))
    valid_data = CIFAR10DataProvider('valid', int(batch_size))
    output_dimension = 10

elif(dataset == 'cifar100'):    
    train_data = CIFAR100DataProvider('train', int(batch_size))
    valid_data = CIFAR100DataProvider('valid', int(batch_size))
    output_dimension = 100


elif(dataset == 'mnist'):
    train_data = data_providers.MNISTDataProvider('train', int(batch_size))
    valid_data = data_providers.MNISTDataProvider('valid', int(batch_size))   
    output_dimension = 10 

# below code will work only for the CIFAR10 and CIFAR100 datasets, because mnist has a different input dimension
graph = tf.Graph()

with graph.as_default():
    inputs = tf.placeholder(tf.float32, [None, train_data.inputs.shape[1]], 'inputs')
    targets = tf.placeholder(tf.float32, [None, train_data.num_classes], 'targets')
    keep_prob = tf.placeholder(tf.float32)    # there is only 1 because it will be the same for all 3 dropout layers

    # this is the actual model
    with tf.name_scope('convolutional-layer-1'):
        weights_conv1 = weight_variable([patch_size, patch_size, 3, num_channels1])    # the patch of the image we look at is 5x5, there are 3 input channels (RGB),
        biases_conv1 = bias_variable([num_channels1])                # and the number of output channels will be 32 (investigate 32 features)
        
        inputs_reshaped = tf.reshape(inputs,[-1,3,32,32])
        image_trans = tf.transpose(inputs_reshaped,[0,2,3,1])
        
        # h_conv1 = tf.nn.relu(conv2d(image_trans, weights_conv1) + biases_conv1)
        h_conv_no_act = conv2d(image_trans, weights_conv1) + biases_conv1
       
        """
        # will now apply batch normalization
        outputs_bn = 0.
        if(isTrain == True):
            outputs_bn = batch_norm(h_conv_no_act,num_channels1)
        else:
            outputs_bn = batch_norm(h_conv_no_act,num_channels1, use_global=True) 
        """
        h_conv1 = tf.nn.relu(h_conv_no_act)
        h_pool1 = max_pool_2x2(h_conv1)      # reduces it to 16x16x32 (32 channels)      
       
    with tf.name_scope('dropout-layer-1'):
        h_cl1_drop = tf.nn.dropout(h_pool1, keep_prob)
    

    with tf.name_scope('convolutional-layer-2'):
        weights_conv2 = weight_variable([patch_size, patch_size, num_channels1, num_channels2])
        biases_conv2 = bias_variable([num_channels2])
        
        h_conv2_no_act = conv2d(h_cl1_drop, weights_conv2) + biases_conv2
        """
        # will now apply batch normalization
        outputs_bn = 0.
        if(isTrain == True):
            outputs_bn = batch_norm(h_conv2_no_act,num_channels2)
        else:
            outputs_bn = batch_norm(h_conv2_no_act,num_channels2, use_global=True) 
        """
        h_conv2 = tf.nn.relu(h_conv2_no_act)
        h_pool2 = max_pool_2x2(h_conv2)      # reduces it to 8x16x32 (32 channels) 
    
    
    with tf.name_scope('dropout-layer-2'):
        h_cl2_drop = tf.nn.dropout(h_pool2, keep_prob)
    

    with tf.name_scope('fully-connected-layer'):
        h_pool2_last = tf.transpose(h_cl2_drop,[0,3,1,2])
        h_pool2_trans_last = tf.reshape(h_pool2_last,[-1,num_channels2,8,8])
        final = tf.reshape(h_pool2_trans_last,[-1,8*8*num_channels2])
        weights_fc1 = weight_variable([8 * 8 * num_channels2, hidden_dimension])   # 1500 will be the hidden dimension   
        biases_fc1 = bias_variable([hidden_dimension])
        h_fc1 = 0
        if(isTrain == True):
            h_fc1 = fully_connected_layer_cnn_batch_norm(final, weights_fc1, biases_fc1, hidden_dimension)
            #online_variance(h_fc1)
        else:
            h_fc1 = fully_connected_layer_cnn_batch_norm(final, weights_fc1, biases_fc1,hidden_dimension)
        

    with tf.name_scope('dropout-layer-3'):
        h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)
    

    with tf.name_scope('output-layer'):
        weights_fc2 = weight_variable([hidden_dimension, output_dimension])
        biases_fc2 = bias_variable([output_dimension])
        outputs = fully_connected_layer_cnn(h_fc1_drop, weights_fc2, biases_fc2, tf.identity)   # no activation function

    
    with tf.name_scope('error'):
        error = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(outputs, targets))
    

    with tf.name_scope('accuracy'):
        accuracy = tf.reduce_mean(tf.cast(
                tf.equal(tf.argmax(outputs, 1), tf.argmax(targets, 1)), 
                tf.float32))
    

    with tf.name_scope('train'):
        train_step = tf.train.GradientDescentOptimizer(learning_rate = 0.01).minimize(error)
    
    
    with graph.as_default():
        tf.summary.scalar('error', error)
        tf.summary.scalar('accuracy', accuracy)
        summary_op = tf.summary.merge_all()

    timestamp = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    train_writer = tf.summary.FileWriter(os.path.join('graphs/tf-log', timestamp, 'train'), graph=graph)
    valid_writer = tf.summary.FileWriter(os.path.join('graphs/tf-log', timestamp, 'valid'), graph=graph)

    with graph.as_default():
        init = tf.global_variables_initializer()

    num_epochs = 100
    sess = tf.InteractiveSession(graph=graph)
    sess.run(init)

    train_data_errors = {}
    train_data_accuracies = {}
    valid_data_errors = {}
    valid_data_accuracies = {}

    for e in range(num_epochs):
        running_error = 0.
        running_accuracy = 0.
        batch_counter = 0
        start = time.time()
        for b, (input_batch, target_batch) in enumerate(train_data):
            # need to go into batch and extract each data point
            global isTrain
            isTrain = True
            _, batch_error, batch_acc, summary = sess.run(
                [train_step, error, accuracy, summary_op], 
                feed_dict={inputs: input_batch, targets: target_batch, keep_prob: kp})
            running_error += batch_error
            running_accuracy += batch_acc
            train_writer.add_summary(summary, e * train_data.num_batches + b)
            batch_counter = batch_counter + 1
            end = time.time()
            start = time.time()

        running_error /= train_data.num_batches
        running_accuracy /= train_data.num_batches
        train_data_errors[e+1] = running_error
        train_data_accuracies[e+1] = running_accuracy

        print('End of epoch {0:02d}: err(train)={1:.2f} acc(train)={2:.2f}'
              .format(e + 1, running_error, running_accuracy))
        if (e + 1) % 5 == 0:
            valid_error = 0.
            valid_accuracy = 0.
            for b,(input_batch, target_batch) in enumerate(valid_data):
                global isTrain
                isTrain = False
                batch_error, batch_acc, valid_summary = sess.run(
                    [error, accuracy, summary_op], 
                    feed_dict={inputs: input_batch, targets: target_batch, keep_prob: 1})
                valid_error += batch_error
                valid_accuracy += batch_acc
                valid_writer.add_summary(valid_summary, e * train_data.num_batches + b)
            valid_error /= valid_data.num_batches
            valid_accuracy /= valid_data.num_batches
            valid_data_errors[e+1] = valid_error
            valid_data_accuracies[e+1] = valid_accuracy
            print('                 err(valid)={0:.2f} acc(valid)={1:.2f}'
                   .format(valid_error, valid_accuracy))
        
        global global_n, global_mean, global_sum, global_variance, int_counter
        global_mean = tf.zeros([3072])
        global_sum = tf.zeros([3072])
        global_n = tf.zeros([3072])
        global_variance = tf.zeros([3072])
        global_int_counter = 0

    allDicts = [train_data_errors,train_data_accuracies,valid_data_errors,valid_data_accuracies]
    fileName = 'num_channels_experiment_' + str(num_channels1) + '.txt'
    outputFile = open(fileName, "w")
    outputFile.write(str(allDicts))
    outputFile.flush()
    outputFile.close()
