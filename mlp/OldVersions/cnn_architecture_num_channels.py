import os
import tensorflow as tf
import numpy as np
from mlp.data_providers import CIFAR10DataProvider, CIFAR100DataProvider
import matplotlib.pyplot as plt
import sys
import data_providers
import tensorflowlayers
import datetime


def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)

def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
  return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='SAME')

def find_mean(x,y):
    sum = x + y
    mean = sum / 2
    return mean

commandLineArguments = sys.argv
print(commandLineArguments)
if len(commandLineArguments) != 3:     # 3 because the file name is also an argument
    raise ValueError('Must have exactly 2 arguments - dataset and batch_size')

dataset = commandLineArguments[1]
batch_size = commandLineArguments[2]
ouput_dim = 0


if(dataset == 'cifar10'):   
    train_data = CIFAR10DataProvider('train', int(batch_size))
    valid_data = CIFAR10DataProvider('valid', int(batch_size))
    output_dim = 10
 
elif(dataset == 'cifar100'):    
    train_data = CIFAR100DataProvider('train', int(batch_size))
    valid_data = CIFAR100DataProvider('valid', int(batch_size))
    output_dim = 100

elif(dataset == 'mnist'):
    train_data = data_providers.MNISTDataProvider('train', int(batch_size))
    valid_data = data_providers.MNISTDataProvider('valid', int(batch_size))    
    output_dim = 10

# number_channels will represent the number of channels outputed from the first convolutional layer, 
# then the second convolutional layer will output twice that many channels
# code below is hard-coded for 32x32x3, so will only work for cifar10 and cifar100 
for num_channels in [4,8,16,64]:
    print('Number of channels in the first conv layer is ' + str(num_channels))
    twice_num_channels = 2 * num_channels
    print('Number of channels in the second conv layer is ' + str(twice_num_channels))
    graph = tf.Graph()

    with graph.as_default():
        inputs = tf.placeholder(tf.float32, [None, train_data.inputs.shape[1]], 'inputs')
        targets = tf.placeholder(tf.float32, [None, train_data.num_classes], 'targets')

        # this is the actual model
        with tf.name_scope('convolutional-layer-1'):
            weights_conv1 = weight_variable([5, 5, 3, num_channels])    # the patch of the image we look at is 5x5, there are 3 input channels (RGB),
            biases_conv1 = bias_variable([num_channels])                # and the number of output channels will be num_channels
            x_image = tf.reshape(inputs,[-1,32,32,3])         # image is 32x32, and there are 3 input channels(RGB)
            h_conv1 = tf.nn.relu(conv2d(x_image, weights_conv1) + biases_conv1)
            h_pool1 = max_pool_2x2(h_conv1)                   # reduces it to 16x16xnum_channels


        with tf.name_scope('convolutional-layer-2'):
            weights_conv2 = weight_variable([5, 5, num_channels, twice_num_channels])
            biases_conv2 = bias_variable([twice_num_channels])
            h_conv2 = tf.nn.relu(conv2d(h_pool1, weights_conv2) + biases_conv2)
            h_pool2 = max_pool_2x2(h_conv2)         # reduces it to 8x8xtwice_num_channels
        


        hidden_dimension = find_mean(8*8*twice_num_channels,output_dim)

        with tf.name_scope('fully-connected-layer'):
            weights_fc1 = weight_variable([8 * 8 * twice_num_channels, hidden_dimension])   # 2503 will be the hidden dimension - mean of 8*8*64 and 10 ()    
            biases_fc1 = bias_variable([hidden_dimension])
            h_pool2_flat = tf.reshape(h_pool2, [-1, 8*8*twice_num_channels])
            h_fc1 = tensorflowlayers.fully_connected_layer_cnn(h_pool2_flat, weights_fc1, biases_fc1)


        with tf.name_scope('dropout-layer'):
            keep_prob = tf.placeholder(tf.float32)
            h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)
        

        with tf.name_scope('output-layer'):
            weights_fc2 = weight_variable([hidden_dimension, output_dim])
            biases_fc2 = bias_variable([output_dim])
            outputs = tensorflowlayers.fully_connected_layer_cnn(h_fc1_drop, weights_fc2, biases_fc2, tf.identity)   # no activation function

        
        with tf.name_scope('error'):
            error = tf.reduce_mean(
                tf.nn.softmax_cross_entropy_with_logits(outputs, targets))
        

        with tf.name_scope('accuracy'):
            accuracy = tf.reduce_mean(tf.cast(
                    tf.equal(tf.argmax(outputs, 1), tf.argmax(targets, 1)), 
                    tf.float32))
        

        with tf.name_scope('train'):
            train_step = tf.train.GradientDescentOptimizer(learning_rate = 0.01).minimize(error)
        
    with graph.as_default():
        tf.summary.scalar('error', error)
        tf.summary.scalar('accuracy', accuracy)
        summary_op = tf.summary.merge_all()

    timestamp = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    train_writer = tf.summary.FileWriter(os.path.join('graphs/tf-log', timestamp, 'train'), graph=graph)
    valid_writer = tf.summary.FileWriter(os.path.join('graphs/tf-log', timestamp, 'valid'), graph=graph)

    with graph.as_default():
        init = tf.global_variables_initializer()

    num_epochs = 100
    sess = tf.InteractiveSession(graph=graph)
    sess.run(init)

    for e in range(num_epochs):
        running_error = 0.
        running_accuracy = 0.
        for b, (input_batch, target_batch) in enumerate(train_data):
            _, batch_error, batch_acc, summary = sess.run(
                [train_step, error, accuracy, summary_op], 
                feed_dict={inputs: input_batch, targets: target_batch, keep_prob: 0.45})   
            running_error += batch_error
            running_accuracy += batch_acc
            train_writer.add_summary(summary, e * train_data.num_batches + b)
        running_error /= train_data.num_batches
        running_accuracy /= train_data.num_batches
        print('End of epoch {0:02d}: err(train)={1:.2f} acc(train)={2:.2f}'
              .format(e + 1, running_error, running_accuracy))
        if (e + 1) % 5 == 0:
            valid_error = 0.
            valid_accuracy = 0.
            for b,(input_batch, target_batch) in enumerate(valid_data):
                batch_error, batch_acc, valid_summary = sess.run(
                    [error, accuracy, summary_op], 
                    feed_dict={inputs: input_batch, targets: target_batch, keep_prob: 1})
                valid_error += batch_error
                valid_accuracy += batch_acc
                valid_writer.add_summary(valid_summary, e * train_data.num_batches + b)
            valid_error /= valid_data.num_batches
            valid_accuracy /= valid_data.num_batches
            print('                 err(valid)={0:.2f} acc(valid)={1:.2f}'
                   .format(valid_error, valid_accuracy))
