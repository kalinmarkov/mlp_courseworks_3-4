import os
import tensorflow as tf
import numpy as np
from mlp.data_providers import CIFAR10DataProvider, CIFAR100DataProvider
import matplotlib.pyplot as plt
import sys
import data_providers
import tensorflowlayers
import datetime

commandLineArguments = sys.argv
print(commandLineArguments)
if len(commandLineArguments) != 3:     # 3 because the file name is also an argument
    raise ValueError('Must have exactly 2 arguments - dataset and batch_size')

dataset = commandLineArguments[1]
batch_size = commandLineArguments[2]

if(dataset == 'cifar10'):   
    train_data = CIFAR10DataProvider('train', int(batch_size))
    valid_data = CIFAR10DataProvider('valid', int(batch_size))

elif(dataset == 'cifar100'):    
    train_data = CIFAR100DataProvider('train', int(batch_size))
    valid_data = CIFAR100DataProvider('valid', int(batch_size))

elif(dataset == 'mnist'):
    train_data = data_providers.MNISTDataProvider('train', int(batch_size))
    valid_data = data_providers.MNISTDataProvider('valid', int(batch_size))    

"""
We will try the following optimizers:
1) Momentum Learning Rule with momentum term set = 0.9 and learning rate = 0.01
2) Adagrad Learning Rule with learning rate = 0.01
3) Adam Learning Rule with learning_rate = 0.001, beta1 = 0.9, beta2 = 0.999 (the default parameters)

Everything else will be held constant. So there will be three graphs - they will be mostly the same but 
only differ in the optimizers they use to train the neural network.
"""

for optimizer in ['momentum','adagrad','adam']:
    graph = tf.Graph()

    with graph.as_default():
        inputs = tf.placeholder(tf.float32, [None, train_data.inputs.shape[1]], 'inputs')
        targets = tf.placeholder(tf.float32, [None, train_data.num_classes], 'targets')
        num_hidden = 200        # dimension of hidden layers

        # this is the actual model
        with tf.name_scope('fc-layer-1'):
            hidden_1 = tensorflowlayers.fully_connected_layer(inputs, train_data.inputs.shape[1], num_hidden)   # activation function is RELU
        with tf.name_scope('fc-layer-2'):
            hidden_2 = tensorflowlayers.fully_connected_layer(hidden_1, num_hidden, num_hidden)   # activation function is RELU
        with tf.name_scope('output-layer'):
            outputs = tensorflowlayers.fully_connected_layer(hidden_2, num_hidden, train_data.num_classes, tf.identity)   # no activation function
        with tf.name_scope('error'):
            error = tf.reduce_mean(
                tf.nn.softmax_cross_entropy_with_logits(outputs, targets))
        with tf.name_scope('accuracy'):
            accuracy = tf.reduce_mean(tf.cast(
                    tf.equal(tf.argmax(outputs, 1), tf.argmax(targets, 1)), 
                    tf.float32))
        with tf.name_scope('train'):
            if(optimizer == 'momentum'):
                train_step = tf.train.MomentumOptimizer(learning_rate = 0.01, momentum = 0.9).minimize(error)
                print("Trained using momentum")
            if(optimizer == 'adagrad'):
                train_step = tf.train.AdagradOptimizer(learning_rate = 0.01).minimize(error)
                print("Trained using adagrad")
            if(optimizer == 'adam'):
                train_step = tf.train.AdamOptimizer().minimize(error)
                print("Trained using adam")
        
    with graph.as_default():
        tf.summary.scalar('error', error)
        tf.summary.scalar('accuracy', accuracy)
        summary_op = tf.summary.merge_all()

    timestamp = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    if(optimizer == 'momentum'):
        train_writer = tf.summary.FileWriter(os.path.join('graphs/tf-log/momentum', timestamp, 'train'), graph=graph)
        valid_writer = tf.summary.FileWriter(os.path.join('graphs/tf-log/momentum', timestamp, 'valid'), graph=graph)
    if(optimizer == 'adagrad'):
        train_writer = tf.summary.FileWriter(os.path.join('graphs/tf-log/adagrad', timestamp, 'train'), graph=graph)
        valid_writer = tf.summary.FileWriter(os.path.join('graphs/tf-log/adagrad', timestamp, 'valid'), graph=graph)
    if(optimizer == 'adam'):
        train_writer = tf.summary.FileWriter(os.path.join('graphs/tf-log/adam', timestamp, 'train'), graph=graph)
        valid_writer = tf.summary.FileWriter(os.path.join('graphs/tf-log/adam', timestamp, 'valid'), graph=graph)
    
    with graph.as_default():
        init = tf.global_variables_initializer()

    num_epochs = 100
    sess = tf.InteractiveSession(graph=graph)
    sess.run(init)

    for e in range(num_epochs):
        running_error = 0.
        running_accuracy = 0.
        for b, (input_batch, target_batch) in enumerate(train_data):
            _, batch_error, batch_acc, summary = sess.run(
                [train_step, error, accuracy, summary_op], 
                feed_dict={inputs: input_batch, targets: target_batch})
            running_error += batch_error
            running_accuracy += batch_acc
            train_writer.add_summary(summary, e * train_data.num_batches + b)
        running_error /= train_data.num_batches
        running_accuracy /= train_data.num_batches
        print('End of epoch {0:02d}: err(train)={1:.2f} acc(train)={2:.2f}'
              .format(e + 1, running_error, running_accuracy))
        if (e + 1) % 5 == 0:
            valid_error = 0.
            valid_accuracy = 0.
            for b,(input_batch, target_batch) in enumerate(valid_data):
                batch_error, batch_acc, valid_summary = sess.run(
                    [error, accuracy, summary_op], 
                    feed_dict={inputs: input_batch, targets: target_batch})
                valid_error += batch_error
                valid_accuracy += batch_acc
                valid_writer.add_summary(valid_summary, e * train_data.num_batches + b)
            valid_error /= valid_data.num_batches
            valid_accuracy /= valid_data.num_batches
            print('                 err(valid)={0:.2f} acc(valid)={1:.2f}'
                   .format(valid_error, valid_accuracy))
