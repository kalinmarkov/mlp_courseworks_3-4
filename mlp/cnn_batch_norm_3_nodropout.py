import os
import tensorflow as tf
import numpy as np
from mlp.data_providers import CIFAR10DataProvider, CIFAR100DataProvider
import matplotlib.pyplot as plt
import sys
import data_providers
import tensorflowlayers
import datetime
import cPickle as pickle
import time

# all global variables declared here
commandLineArguments = sys.argv
print(commandLineArguments)
dataset = commandLineArguments[1]
batch_size = commandLineArguments[2]
output_dimension = 0

patch_size = 5
hidden_dimension = 1500
kp = 0.8
num_channels1 = 32
num_channels2 = 16

def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)

def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
  return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='SAME')

def fully_connected_layer_cnn(inputs, weights, biases, nonlinearity=tf.nn.relu):
    outputs_no_activation = tf.matmul(inputs, weights) + biases
    outputs = nonlinearity(outputs_no_activation)
    return outputs

def fully_connected_layer_cnn_batch_norm(inputs, weights, biases, phase, nonlinearity=tf.nn.relu):
    outputs_no_activation = tf.matmul(inputs, weights) + biases
    outputs_bn = tf.contrib.layers.batch_norm(outputs_no_activation, center=True, scale=True, is_training=phase, scope='bn')

    outputs = nonlinearity(outputs_bn)
    return outputs

if len(commandLineArguments) != 3:     # 3 because the file name is also an argument
    raise ValueError('Must have exactly 2 arguments - dataset and batch_size')


if(dataset == 'cifar10'):
    train_data = CIFAR10DataProvider('train', int(batch_size))
    valid_data = CIFAR10DataProvider('valid', int(batch_size))
    output_dimension = 10

elif(dataset == 'cifar100'):
    train_data = CIFAR100DataProvider('train', int(batch_size))
    valid_data = CIFAR100DataProvider('valid', int(batch_size))
    output_dimension = 100


elif(dataset == 'mnist'):
    train_data = data_providers.MNISTDataProvider('train', int(batch_size))
    valid_data = data_providers.MNISTDataProvider('valid', int(batch_size))
    output_dimension = 10

# below code will work only for the CIFAR10 and CIFAR100 datasets, because mnist has a different input dimension
graph = tf.Graph()

with graph.as_default():
    inputs = tf.placeholder(tf.float32, [None, train_data.inputs.shape[1]], 'inputs')
    targets = tf.placeholder(tf.float32, [None, train_data.num_classes], 'targets')
    keep_prob = tf.placeholder(tf.float32)    # there is only 1 because it will be the same for all 3 dropout layers
    phase = tf.placeholder(tf.bool, name='phase')

    normalizer_fn = tf.contrib.layers.batch_norm
    normalizer_params = {"scale" : True, "is_training" : phase, "scope": 'bn'}

    # this is the actual model
    with tf.name_scope('convolutional-layer-1'):
        weights_conv1 = weight_variable([patch_size, patch_size, 3, num_channels1])
        #biases_conv1 = bias_variable([num_channels1])

        inputs_reshaped = tf.reshape(inputs,[-1,3,32,32])
        image_trans = tf.transpose(inputs_reshaped,[0,2,3,1])

        temp1 = tf.contrib.layers.convolution2d(inputs=image_trans,num_outputs=num_channels1,kernel_size=[patch_size,patch_size],
                                               padding='SAME', activation_fn=tf.nn.relu,normalizer_fn=normalizer_fn,
                                               normalizer_params=normalizer_params,scope='cnn1')

        h_pool1 = max_pool_2x2(temp1)
        print('Shape after conv1 is: ' + str(h_pool1.get_shape()))

    with tf.name_scope('convolutional-layer-2'):
        weights_conv2 = weight_variable([patch_size, patch_size, num_channels1, num_channels2])
        #biases_conv2 = bias_variable([num_channels2])

        temp2 = tf.contrib.layers.convolution2d(inputs=h_pool1,num_outputs=num_channels2,kernel_size=[patch_size,patch_size],
                                               padding='SAME', activation_fn=tf.nn.relu,normalizer_fn=normalizer_fn,
                                               normalizer_params=normalizer_params,scope='cnn2')
        h_pool2 = max_pool_2x2(temp2)
        print('Shape after conv2 is: ' + str(h_pool2.get_shape()))


    with tf.name_scope('fully-connected-layer'):
        h_pool2_last = tf.transpose(h_pool2,[0,3,1,2])
        h_pool2_trans_last = tf.reshape(h_pool2_last,[-1,num_channels2,8,8])
        final = tf.reshape(h_pool2_trans_last,[-1,8*8*num_channels2])
        weights_fc1 = weight_variable([8 * 8 * num_channels2, hidden_dimension])   # 1500 will be the hidden dimension
        biases_fc1 = bias_variable([hidden_dimension])
        h_fc1 = fully_connected_layer_cnn_batch_norm(final, weights_fc1, biases_fc1, phase)


    with tf.name_scope('output-layer'):
        weights_fc2 = weight_variable([hidden_dimension, output_dimension])
        biases_fc2 = bias_variable([output_dimension])
        outputs = fully_connected_layer_cnn(h_fc1, weights_fc2, biases_fc2, tf.identity)   # no activation function


    with tf.name_scope('error'):
        error = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(outputs, targets))


    with tf.name_scope('accuracy'):
        accuracy = tf.reduce_mean(tf.cast(
                tf.equal(tf.argmax(outputs, 1), tf.argmax(targets, 1)),
                tf.float32))

    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)

    with tf.control_dependencies(update_ops):
        # Ensures that we execute the update_ops before performing the train_step
        train_step = tf.train.GradientDescentOptimizer(learning_rate = 0.01).minimize(error)

    with graph.as_default():
        tf.summary.scalar('error', error)
        tf.summary.scalar('accuracy', accuracy)
        summary_op = tf.summary.merge_all()

    timestamp = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    train_writer = tf.summary.FileWriter(os.path.join('graphs/tf-log', timestamp, 'train'), graph=graph)
    valid_writer = tf.summary.FileWriter(os.path.join('graphs/tf-log', timestamp, 'valid'), graph=graph)

    with graph.as_default():
        init = tf.global_variables_initializer()

    num_epochs = 100
    sess = tf.InteractiveSession(graph=graph)
    sess.run(init)

    train_data_errors = {}
    train_data_accuracies = {}
    valid_data_errors = {}
    valid_data_accuracies = {}

    for e in range(num_epochs):
        running_error = 0.
        running_accuracy = 0.
        batch_counter = 0
        start = time.time()
        for b, (input_batch, target_batch) in enumerate(train_data):
            #print('Batch ' + str(batch_counter))
            _, batch_error, batch_acc, summary = sess.run(
                [train_step, error, accuracy, summary_op],
                feed_dict={inputs: input_batch, targets: target_batch, keep_prob: kp, phase: 1})
            running_error += batch_error
            running_accuracy += batch_acc
            train_writer.add_summary(summary, e * train_data.num_batches + b)
            batch_counter = batch_counter + 1

        running_error /= train_data.num_batches
        running_accuracy /= train_data.num_batches
        train_data_errors[e+1] = running_error
        train_data_accuracies[e+1] = running_accuracy

        print('End of epoch {0:02d}: err(train)={1:.2f} acc(train)={2:.2f}'
              .format(e + 1, running_error, running_accuracy))
        if (e + 1) % 5 == 0:
            valid_error = 0.
            valid_accuracy = 0.
            for b,(input_batch, target_batch) in enumerate(valid_data):
                batch_error, batch_acc, valid_summary = sess.run(
                    [error, accuracy, summary_op],
                    feed_dict={inputs: input_batch, targets: target_batch, keep_prob: 1, phase: 0})
                valid_error += batch_error
                valid_accuracy += batch_acc
                valid_writer.add_summary(valid_summary, e * train_data.num_batches + b)
            valid_error /= valid_data.num_batches
            valid_accuracy /= valid_data.num_batches
            valid_data_errors[e+1] = valid_error
            valid_data_accuracies[e+1] = valid_accuracy
            print('                 err(valid)={0:.2f} acc(valid)={1:.2f}'
                   .format(valid_error, valid_accuracy))

        end = time.time()
        time_elapsed = end - start
        print('Time per epoch ' + str(time_elapsed))

    allDicts = [train_data_errors,train_data_accuracies,valid_data_errors,valid_data_accuracies]
    fileName = 'batch_norm_experiment_nodropout.txt'
    outputFile = open(fileName, "w")
    outputFile.write(str(allDicts))
    outputFile.flush()
