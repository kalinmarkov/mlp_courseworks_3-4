import ast
import sys
import matplotlib.pyplot as plt
from collections import OrderedDict

commandLineArguments = sys.argv
file_to_graph = commandLineArguments[1]

# Graph for training set error
fig_1 = plt.figure()
ax_1 = fig_1.add_subplot(111)


# Graph for training set accuracy
fig_2 = plt.figure()
ax_2 = fig_2.add_subplot(111)


# Graph for validation set error
fig_3 = plt.figure()
ax_3 = fig_3.add_subplot(111) 

    
# Graph for validation set accuracy
fig_4 = plt.figure()
ax_4 = fig_4.add_subplot(111)

ax_1.set_xlabel('Epoch number')
ax_1.set_ylabel('Error Rate')
ax_1.set_title('Training Set Error')

  
ax_2.set_xlabel('Epoch number')
ax_2.set_ylabel('Accuracy Rate')
ax_2.set_title('Training Set Accuracy')


ax_3.set_xlabel('Epoch number')
ax_3.set_ylabel('Error Rate')
ax_3.set_title('Validation Set Error')


ax_4.set_xlabel('Epoch number')
ax_4.set_ylabel('Accuracy Rate')
ax_4.set_title('Validation Set Accuracy')




files_to_graph = []
model = ['Baseline','Experiment1&2&3','Experiment 4']

for i in range(len(commandLineArguments)-1):
    files_to_graph.append(commandLineArguments[i+1])
    inputFile = open(files_to_graph[i], "r")
    lines = inputFile.readlines()

    objects = []
    for line in lines:
    	objects.append( ast.literal_eval(line) )

    myDicts = objects[0]
    train_data_errors = myDicts[0]
    ax_1.plot(range(len(train_data_errors)), train_data_errors.values(), label=model[i])
    
    train_data_accuracies = myDicts[1]
    ax_2.plot(range(len(train_data_accuracies)), train_data_accuracies.values(), label=model[i])

    valid_data_errors = OrderedDict(sorted(myDicts[2].items(), key=lambda t: t[0]))
    ax_3.plot(valid_data_errors.keys(), valid_data_errors.values(), label=model[i])
    
    valid_data_accuracies = OrderedDict(sorted(myDicts[3].items(), key=lambda t: t[0]))
    ax_4.plot(valid_data_accuracies.keys(), valid_data_accuracies.values(), label=model[i])

ax_1.legend(loc=0)
ax_2.legend(loc=0)
ax_3.legend(loc=0)
ax_4.legend(loc=0)

fig_1.savefig('train_data_errors_cifar100')
fig_2.savefig('train_data_accuracies_cifar100')
fig_3.savefig('valid_data_errors_cifar100')
fig_4.savefig('valid_data_accurcacies_cifar100')
